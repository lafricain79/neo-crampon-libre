\c 14
\p
\v 1 En voici un autre qui pense à prendre la mer, et se dispose à voyager sur les flots en fureur : il invoque un bois plus fragile encore que le vaisseau qui le porte ;
\v 2 car, ce vaisseau, c’est la passion du lucre qui l’a inventé, et c’est l’habileté de l’ouvrier qui l’a construit.
\v 3 Mais, ô Père, c’est votre providence qui le gouverne, vous qui avez même ouvert un chemin dans la mer, et une route sûre au milieu des flots,
\v 4 montrant par là que vous pouvez délivrer de tout péril, afin que, même sans la science de la navigation, on puisse se mettre en mer. Vous ne voulez pas que les œuvres de votre sagesse restent inutiles ; c’est pourquoi les hommes, confiant leur vie à un bois fragile,
\v 5 traversent les vagues sur un radeau, et échappent à la mort.
\v 6 Et jadis, alors que les géants orgueilleux périssaient, l’espérance de l’univers échappa sur une barque, et, gouvernée par votre main, laissa au monde la semence d’une postérité.
\v 7 Car béni est le bois qui sert à un juste usage.
\p
\v 8 Mais l’idole, œuvre de la main des hommes, est maudite, elle et son auteur : celui-ci parce qu’il l’a faite, celle-là parce qu’étant périssable, elle est appelée dieu ;
\v 9 car Dieu hait également l’impie et son impiété,
\v 10 et l’œuvre et l’ouvrier seront pareillement châtiés.
\v 11 C’est pourquoi les idoles des nations seront visitées, parce que, créatures de Dieu, elles sont devenues une abomination, un scandale pour les âmes des hommes, un piège pour les pieds des insensés.
\p
\v 12 L’idée de faire des idoles fut le principe de la fornication, et leur invention a amené la perte de la vie.
\v 13 Il n’y en avait pas à l’origine et il n’y en aura pas toujours.
\v 14 C’est la vanité des hommes qui les a introduites dans le monde ; aussi leur fin prochaine est-elle arrêtée dans la pensée divine.
\p
\v 15 Un père accablé par une douleur prématurée a façonné l’image d’un fils qui lui a été trop tôt enlevé ; et cet enfant qui était mort, il s’est mis à l’honorer comme un dieu, et il a institué parmi les gens de sa maison des rites pieux et des cérémonies.
\v 16 Puis, cette coutume impie, s’affermissant avec le temps, fut observée comme une loi, et, sur l’ordre des princes, on adora les statues.
\p
\v 17 Quand on ne pouvait les honorer en face, parce qu’ils habitaient trop loin, on se représentait leur lointaine figure, et l’on façonnait une image visible du roi vénéré, afin de rendre à l’absent des hommages aussi empressés que s’il eût été présent.
\v 18 Et, pour le succès de la superstition, ceux qui ne le connaissaient pas y furent amenés par l’ambition de l’artiste.
\v 19 Celui-ci, en effet, désireux de plaire au maître puissant, épuisa tout son art à embellir le portrait.
\v 20 Et la foule des hommes, séduite par l’élégance de l’œuvre, regarda comme un dieu celui qui naguère était honoré comme un homme.
\p
\v 21 Ce fut un piège pour les vivants que les hommes, sous l’influence de l’infortune ou de la tyrannie, eussent donné à la pierre ou au bois le nom incommunicable.
\v 22 Bientôt ce ne fut pas assez pour eux d’errer dans la notion de Dieu ; vivant dans un état de lutte violente, par suite de leur ignorance, ils appelaient du nom de paix de tels maux.
\p
\v 23 Célébrant des cérémonies homicides de leurs enfants ou des mystères clandestins, et se livrant aux débauches effrénées de rites étranges,
\v 24 ils n’ont plus gardé de pudeur ni dans leur vie, ni dans leurs mariages. L’un tue l’autre par la trahison, ou l’outrage par l’adultère.
\v 25 C’est partout un mélange de sang et de meurtre, de vol et de tromperie, de corruption et d’infidélité, de révolte et de parjure,
\v 26 de persécution des gens de bien, d’oubli des bienfaits, de souillure des âmes, de crimes contre nature, d’instabilité dans les unions, d’adultère et d’impudicité.
\v 27 Car le culte des idoles sans nom est le principe, la cause et la fin de tout mal.
\v 28 Leurs divertissements sont de folles joies, et leurs oracles, des mensonges ; ils vivent dans l’injustice et se parjurent sans scrupule.
\v 29 Comme ils mettent leur confiance en des idoles sans vie, ils n’attendent aucun préjudice de leurs parjures.
\p
\v 30 Mais un juste châtiment les frappera pour ce double crime : parce que, s’attachant aux idoles, ils ont eu sur Dieu des pensées perverses, et parce qu’ils ont fait par fourberie des serments contre la justice, au mépris des plus saintes lois.
\v 31 Ce n’est pas la puissance des idoles par lesquelles ils ont juré, c’est le châtiment dû aux pécheurs qui atteint toujours la prévarication des impies.