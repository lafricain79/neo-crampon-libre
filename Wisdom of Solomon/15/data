\c 15
\p
\v 1 Mais vous, ô notre Dieu, vous êtes bon, fidèle et patient, et vous gouvernez tout avec miséricorde.
\v 2 Lors même que nous péchons, nous sommes à vous, connaissant votre puissance ; mais nous ne voulons pas pécher, car nous savons que nous sommes comptés parmi les vôtres.
\v 3 Vous connaître est la justice parfaite, et connaître votre puissance est la racine de l’immortalité.
\v 4 Nous n’avons pas été égarés par l’invention d’un art funeste, ni par une figure barbouillée de diverses couleurs, vain travail d’un peintre :
\v 5 objets dont l’aspect excite la passion de l’insensé, qui s’éprend pour la figure inanimée d’une image sans vie.
\v 6 Affectionnant le mal, ils sont dignes de telles espérances, aussi bien ceux qui les font que ceux qui les aiment ou les adorent.
\p
\v 7 En effet, voici un potier qui pétrit laborieusement la terre molle ; il façonne chaque vase pour notre usage, et de la même argile il fait des vases qui sont destinés à de nobles emplois, et d’autres à des emplois tout contraires, sans distinguer nullement à quel usage chacun d’eux devra servir : c’est le potier qui en est juge.
\v 8 Ensuite, par un travail impie, de la même argile, il façonne une vaine divinité, lui qui, naguère fait de terre, retournera bientôt au lieu d’où il a été tiré, quand on lui redemandera son âme qui lui avait été prêtée.
\v 9 Pourtant il ne s’inquiète pas de ce que ses forces s’épuisent, ni de ce que sa vie est courte ; mais il rivalise avec ceux qui travaillent l’or et l’argent, il imite ceux qui travaillent l’airain, et met sa gloire à exécuter des figures trompeuses.
\v 10 Son cœur est comme de la cendre, son espérance est plus vile que la terre, et sa vie est de moindre valeur que l’argile.
\v 11 Car il méconnaît celui qui l’a fait, qui lui a inspiré une âme agissante, et a mis en lui un souffle de vie.
\v 12 Il regarde notre existence comme un amusement, la vie comme un marché où l’on se rassemble pour le gain ; car, disent-ils, « il faut acquérir par tous les moyens, même par le crime. »
\v 13 Car celui-là sait bien qu’il est plus coupable que tous les autres, qui, de la même terre, façonne des vases fragiles et des idoles.
\p
\v 14 Mais ils sont tous très insensés, et plus malheureux que l’âme d’un enfant, les ennemis de votre peuple qui le tiennent dans l’oppression !
\p
\v 15 Car ils ont regardé comme des dieux toutes les idoles des nations, qui ne peuvent user de leurs yeux pour voir, ni de leurs narines pour respirer l’air, ni de leurs oreilles pour entendre, ni des doigts de leurs mains pour toucher, et dont les pieds sont incapables de marcher.
\v 16 C’est un homme qui les a faites, et c’est celui à qui on a prêté le souffle qui les a façonnées. Il n’est pas d’homme qui puisse faire un dieu semblable à lui,
\v 17 car, étant mortel, il ne fait de ses mains impies qu’une œuvre morte ; il vaut mieux que les objets qu’il adore, car au moins il a la vie, et eux ne l’ont jamais eue.
\p
\v 18 Ils rendent un culte aux animaux les plus odieux, lesquels, jugés d’après la stupidité, sont pires que les autres.
\v 19 Il n’y a rien de bon en eux qui fasse naître l’affection, comme à l’aspect d’autres animaux ; ils échappent à la louange de Dieu et à sa bénédiction.