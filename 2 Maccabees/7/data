\c 7
\p
\v 1 Il arriva aussi qu’on prit sept frères avec leur mère, et que le roi voulut les contraindre, en les déchirant à coups de fouets et de nerfs de bœuf, à manger de la chair de porc, interdite par la loi.
\p
\v 2 L’un d’eux, prenant la parole au nom de tous, dit : « Que demandes-tu, et que veux-tu apprendre de nous ? Nous sommes prêts à mourir plutôt que de transgresser la loi de nos pères. »
\v 3 Le roi, outré de colère, commanda de mettre sur le feu des poêles et des chaudières. Aussitôt qu’elles furent brûlantes,
\v 4 il commanda de couper la langue à celui qui avait parlé au nom de tous, puis de lui enlever la peau de la tête et de lui trancher les extrémités, sous les yeux de ses autres frères et de leur mère.
\v 5 Lorsqu’on l’eut ainsi complètement mutilé, il ordonna qu’on l’approchât du feu, respirant encore, et qu’on le fît rôtir dans la poêle. Pendant que la vapeur de la poêle se répandait au loin, ses frères et leur mère s’exhortaient mutuellement à mourir avec courage :
\v 6 « Le Seigneur Dieu voit, disaient-ils, et il a vraiment compassion de nous, selon que Moïse l’a annoncé, dans le cantique qui proteste en face contre Israël, en disant : Il aura pitié de ses serviteurs. »
\p
\v 7 Le premier étant mort de cette manière, on amena le second pour le supplice, et après lui avoir arraché la peau de la tête avec les cheveux, on lui demanda s’il voulait manger du porc avant d’être torturé dans tous les membres de son corps.
\v 8 Il répondit dans la langue de ses pères : « Non ! » C’est pourquoi il subit à son tour les mêmes tourments que le premier.
\v 9 Au moment de rendre le dernier soupir, il dit : « Scélérat que tu es, tu nous ôtes la vie présente, mais le Roi de l’univers nous ressuscitera pour une vie éternelle, nous qui mourons pour être fidèles à ses lois. »
\p
\v 10 Après lui, on tortura le troisième. A la demande du bourreau, il présenta aussitôt sa langue et tendit intrépidement ses mains,
\v 11 et il dit avec un noble courage : « Je tiens ces membres du Ciel, mais à cause de ses lois je les méprise, et c’est de Lui que j’espère les recouvrer un jour. »
\v 12 Le roi lui-même et ceux qui l’accompagnaient furent frappés du courage de ce jeune homme, qui comptait pour rien les tortures.
\p
\v 13 Lui mort, on fit subir au quatrième les mêmes tourments.
\v 14 Sur le point d’expirer, il dit : « Heureux ceux qui meurent de la main des hommes, avec l’espérance qu’ils tiennent de Dieu d’être ressuscités par lui ! Pour toi, ta résurrection ne sera pas pour la vie. »
\p
\v 15 On amena ensuite le cinquième, et on le tortura. Mais lui, fixant les yeux sur le roi,
\v 16 dit : « Tu as, quoique mortel, pouvoir parmi les hommes, et tu fais ce que tu veux. Mais ne crois pas que notre race soit abandonnée de Dieu.
\v 17 Pour toi, attends, et tu verras sa grande puissance, comme il te tourmentera toi et ta race. »
\p
\v 18 Après lui, on amena le sixième. Près de mourir, il dit : « Ne te fais pas de vaine illusion ; c’est nous-mêmes qui nous sommes attiré ces maux, en péchant contre notre Dieu ; aussi nous est-il arrivé d’étranges calamités.
\v 19 Mais toi, ne t’imagines pas que tu seras impuni, après avoir osé combattre contre Dieu. »
\p
\v 20 La mère, admirable au-dessus de toute expression et digne d’une illustre mémoire, voyant mourir ses sept fils dans l’espace d’un seul jour, le supporta généreusement, soutenue par son espérance dans le Seigneur.
\v 21 Elle exhortait chacun d’eux en la langue de ses pères et, remplie des plus nobles sentiments, elle raffermissait par un mâle courage sa tendresse de femme.
\v 22 Elle leur disait : « Je ne sais comment vous avez apparu dans mes entrailles ; ce n’est pas moi qui vous ai donné l’esprit et la vie ; ce n’est pas moi qui ai assemblé les éléments qui composent votre corps.
\v 23 C’est pourquoi le Créateur du monde, qui a formé l’homme à sa naissance et qui préside à l’origine de toutes choses, vous rendra dans sa miséricorde et l’esprit et la vie, parce que maintenant vous vous méprisez vous-mêmes pour l’amour de sa loi. »
\p
\v 24 Antiochus se crut insulté et soupçonna un outrage dans ces paroles. Comme le plus jeune était encore en vie, non seulement il lui adressa des exhortations, mais il lui promit avec serment de le rendre riche et heureux, s’il abandonnait les lois de ses pères, d’en faire son ami et de lui confier de hauts emplois.
\v 25 Le jeune homme ne prêtant à ces offres aucune attention, le roi appela la mère et l’engagea à donner à l’adolescent des conseils de salut.
\v 26 Lorsqu’il l’eut longtemps exhortée, elle accepta de persuader son fils.
\v 27 S’étant donc penchée vers lui et raillant le tyran cruel, elle parla ainsi dans la langue de ses pères : « Mon fils, aie pitié de moi, qui t’ai porté neuf mois dans mon sein, qui t’ai allaité trois ans, qui t’ai entretenu, nourri et élevé jusqu’à l’âge où tu es.
\v 28 Je t’en conjure, mon enfant, regarde le ciel et la terre, vois tout ce qu’ils contiennent, et sache que Dieu les a créés de rien, et que la race des hommes est arrivée ainsi à l’existence.
\v 29 Ne crains pas ce bourreau, mais sois digne de tes frères et accepte la mort, afin que je te retrouve, avec tes frères, au temps de la miséricorde. »
\p
\v 30 Comme elle parlait encore, le jeune homme dit : « Qu’attendez-vous ? Je n’obéis pas aux ordres du roi ; j’obéis aux prescriptions de la loi qui a été donnée par Moïse à nos pères.
\v 31 Et toi, l’auteur de tous les maux déchaînés sur les Hébreux, tu n’éviteras pas le bras de Dieu.
\v 32 Car c’est à cause de nos péchés que nous souffrons ;
\v 33 et si, pour nous châtier et nous corriger, notre Seigneur, qui est vivant, nous a montré un moment sa colère, il se réconciliera avec ses serviteurs.
\v 34 Mais toi, ô impie et le plus scélérat de tous les hommes, ne t’enorgueillis pas follement, te livrant à de vaines espérances, quand tu lèves la main contre les serviteurs de Dieu ;
\v 35 car tu n’as pas encore échappé au jugement du Dieu tout-puissant qui surveille toutes choses.
\v 36 Nos frères, après avoir enduré une souffrance passagère, sont échus à l’alliance de Dieu pour une vie éternelle ; mais toi, par le jugement de Dieu, tu porteras le juste châtiment de ton orgueil.
\v 37 Quant à moi, ainsi que mes frères, je livre mon corps et ma vie pour les lois de mes pères, suppliant Dieu d’être bientôt propice envers son peuple et de t’amener, par les tourments et la souffrance, à confesser qu’il est le seul Dieu,
\v 38 et puisse, en moi et en mes frères, s’arrêter la colère du Tout-Puissant, justement déchaînée sur toute notre race ! »
\v 39 Le roi, transporté de fureur, sévit contre celui-ci plus cruellement encore que contre les autres, ne pouvant supporter qu’on se jouât de lui.
\v 40 Ainsi mourut ce jeune homme, pur de toute idolâtrie et se confiant entièrement au Seigneur.
\p
\v 41 Enfin la mère mourut la dernière, après ses enfants.
\p
\v 42 Mais en voilà assez au sujet des sacrifices et des excessives cruautés d’Antiochus.