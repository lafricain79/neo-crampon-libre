\c 37
\p
\v 1 Tout ami dit : « Moi aussi je suis ton ami ; » mais tel ami ne l’est que de nom.
\v 2 N’est-ce pas un chagrin jusqu’à la mort, quand un compagnon et un ami se changent en ennemis ?
\v 3 O pensée perverse, d’où es-tu sortie, pour couvrir la terre de tromperie ?
\v 4 Le compagnon d’un ami se réjouit de ses joies, et, au jour de l’adversité, il se tourne contre lui !
\v 5 Un compagnon partage la peine de son ami dans l’intérêt de son ventre, et, en face du combat, il prend son bouclier !
\v 6 N’oublie pas ton ami dans ton cœur, et, au sein de l’opulence, ne perds pas son souvenir.
\p
\v 7 Tout conseiller donne des conseils, mais il en est qui conseillent dans leur propre intérêt.
\v 8 Vis-à-vis d’un conseiller, tiens-toi sur tes gardes, et cherche d’abord à savoir quel est son intérêt, — car c’est pour lui-même qu’il conseillera, — afin qu’il ne jette pas le sort sur toi,
\v 9 et qu’il ne dise pas : « Ta voie est bonne » ; puis il se tiendra de l’autre côté pour voir ce qui t’arrivera.
\v 10 Ne consulte pas un homme qui te regarde en-dessous, et cache ta résolution à celui qui te jalouse.
\v 11 Ne consulte pas une femme sur sa rivale, un lâche sur la guerre, un marchand sur un échange, un acheteur sur une vente, un envieux sur la reconnaissance, un homme sans compassion sur un acte charitable, un paresseux sur un travail quelconque, un mercenaire de la maison sur l’achèvement d’un travail, un esclave paresseux sur une grosse besogne : ne fais fonds sur ces gens pour aucun conseil.
\v 12 Mais sois assidu près d’un homme pieux, que tu connais comme observateur des commandements, dont le cœur est selon ton cœur, et qui, si tu tombes, souffrira avec toi.
\v 13 Ensuite tiens-toi à ce que ton cœur te conseille, car personne ne t’est plus fidèle que lui.
\v 14 Car l’âme de l’homme annonce parfois plus de choses que sept sentinelles postées sur une hauteur pour observer.
\v 15 Et avec tout cela prie le Très-Haut, afin qu’il dirige sûrement ta voie.
\p
\v 16 Avant toute œuvre est la parole : avant toute entreprise, la réflexion.
\v 17 Comme trace du changement du cœur apparaissent quatre choses :
\v 18 le bien et le mal, la vie et la mort ; et la langue est toujours leur maîtresse.
\p
\v 19 Tel homme est prudent et le docteur d’un grand nombre, mais il est inutile à lui-même.
\v 20 Celui qui affecte la sagesse dans ses paroles est odieux ; il finira par manquer de toute nourriture.
\v 21 Car le Seigneur ne lui a pas donné sa faveur, parce qu’il est dépourvu de toute sagesse.
\p
\v 22 Tel sage est sage pour lui-même, et les fruits de son savoir sont assurés sur ses lèvres.
\v 23 L’homme sage instruit son peuple, et les fruits de son savoir sont assurés.
\v 24 L’homme sage est comblé de bénédictions, et tous ceux qui le voient le proclament heureux.
\v 25 La vie de l’homme est d’un nombre restreint de jours, mais les jours d’Israël sont sans nombre.
\v 26 Le sage obtient la confiance au milieu de son peuple, et son nom vivra à jamais.
\p
\v 27 Mon fils, pour ta manière de vivre, consulte ton âme ; vois ce qui lui est nuisible et ne le lui donne pas.
\v 28 Car tout n’est pas bon pour tous, et chacun ne trouve pas son bien-être en tout.
\v 29 Ne sois pas insatiable devant toute friandise, et ne te jette pas avidement sur les mets ;
\v 30 car l’excès de la nourriture amène des incommodités, et l’intempérance conduit jusqu’à la colique.
\v 31 A cause de l’intempérance beaucoup sont morts, mais celui qui s’abstient prolonge sa vie.