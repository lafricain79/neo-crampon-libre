\c 38
\p
\v 1 Rends au médecin pour tes besoins les honneurs qui lui sont dus ; car, lui aussi, c’est le Seigneur qui l’a créé.
\v 2 C’est du Très-Haut, en effet, que vient la guérison, et du roi lui-même il reçoit des présents.
\v 3 La science du médecin élève sa tête, et il est admiré en présence des grands.
\p
\v 4 Le Seigneur fait produire à la terre ses médicaments, et l’homme sensé ne les dédaigne pas.
\v 5 Un bois n’a-t-il pas adouci l’eau amère, afin que sa vertu fût connue de tous ?
\v 6 Il a donné aux hommes la science, pour se glorifier par ses dons merveilleux.
\v 7 Par eux l’homme procure la guérison, et enlève la douleur.
\v 8 Le pharmacien en fait des mixtions, et son œuvre est à peine achevée que par lui le bien-être se répand sur la face de la terre.
\p
\v 9 Mon fils, si tu es malade, ne néglige pas mon conseil, mais prie le Seigneur, et il te guérira.
\v 10 Eloigne la transgression, redresse tes mains, et purifie ton cœur de tout péché.
\v 11 Offre l’encens et l’oblation de farine, et immole de grasses victimes, comme si c’en était fait de toi.
\v 12 Puis donne accès au médecin, car, lui aussi, le Seigneur l’a créé, et qu’il ne s’éloigne pas de toi, car tu as besoin de lui.
\p
\v 13 Il arrive que leurs mains ont du succès,
\v 14 car eux aussi prieront le Seigneur, afin qu’il leur accorde de procurer le repos et la guérison, pour prolonger la vie du malade.
\v 15 Que celui qui pèche devant son Créateur tombe entre les mains du médecin !
\p
\v 16 Mon fils, répands des pleurs sur un mort, et, comme si tu souffrais cruellement, commence la lamentation. Puis donne à son corps les soins qui lui sont dus, et ne néglige pas sa sépulture.
\v 17 Verse des larmes amères, exhale des soupirs brûlants, et fais le deuil, selon qu’il en est digne, un jour ou deux, pour éviter les mauvais propos. Ensuite console-toi, pour éloigner la tristesse ;
\v 18 car de la tristesse peut venir la mort, et le chagrin du cœur abat toute vigueur.
\p
\v 19 Quand on emmène un mort, le chagrin doit passer avec lui, comme la vie du pauvre est contre son cœur.
\v 20 N’abandonne pas ton cœur à la tristesse ; chasse-la, te souvenant de ta fin.
\v 21 Ne l’oublie pas : il n’y a pas de retour ; tu ne seras pas utile au mort, et tu feras du mal à toi-même.
\v 22 Souviens-toi qu’à l’arrêt porté sur lui, le tien sera pareil : « Pour moi hier, pour toi aujourd’hui. »
\v 23 Quand le mort repose, laisse reposer sa mémoire, et console-toi à son sujet, au départ de son esprit.
\p
\v 24 La sagesse du scribe s’acquiert à la faveur du loisir, et celui qui n’a pas à s’occuper d’affaires deviendra sage.
\p
\v 25 Comment deviendrait-il sage celui qui gouverne la charrue, dont l’ambition est de manier, en guise de lance, l’aiguillon ; qui pousse ses bœufs et se mêle à leurs travaux, et ne sait discourir que des petits des taureaux ?
\v 26 Il met tout son cœur à tracer des sillons, un soin vigilant à procurer le fourrage à ses génisses.
\v 27 Il en est de même de tout charpentier et constructeur, qui poursuivent leurs occupations la nuit comme le jour ; de celui qui grave les empreintes des cachets : son application est de varier les figures ; il met son cœur à reproduire le dessin, un soin vigilant à parfaire son ouvrage.
\v 28 Tel est le forgeron assis près de son enclume, et considérant le fer encore brut ; la vapeur du feu fait fondre ses chairs, et il tient bon contre la chaleur de la fournaise ; le bruit du marteau assourdit son oreille, et son œil est fixé sur le modèle de l’ustensile. Il met son cœur à parfaire son œuvre, un soin vigilant à la polir dans la perfection.
\v 29 Tel encore le potier assis à son ouvrage, et tournant la roue avec ses pieds : constamment il est en souci de son travail, et tous ses efforts tendent à fournir un certain nombre de vases.
\v 30 Avec son bras il façonne l’argile, et devant ses pieds il la rend flexible ; il met son cœur à parfaire le vernis, un soin vigilant à nettoyer son four.
\p
\v 31 Ces sortes de gens attendent tout de leurs mains, et chacun d’eux est intelligent dans son métier.
\v 32 Sans eux on ne bâtirait aucune ville, on n’irait pas à l’étranger, on ne voyagerait pas.
\v 33 Mais ils ne seront pas recherchés dans le conseil du peuple, et ils ne se distingueront pas dans l’assemblée ; ils n’auront pas la science de l’alliance du droit ; ils ne prendront pas place sur le siège du juge ; ils n’interpréteront pas la justice et le droit, et on ne les trouvera pas pour énoncer des sentences.
\v 34 Cependant ils soutiennent les choses du temps, et leur prière se rapporte aux travaux de leur métier.