\c 23
\p
\v 1 Seigneur, Père et souverain Maître de ma vie, ne m’abandonnez pas au conseil de mes lèvres, et ne permettez pas que j’y trouve une occasion de chute.
\v 2 Qui fera sentir le fouet à mes pensées, et la discipline de la sagesse à mon cœur, pour ne pas m’épargner dans mes folies, et ne pas laisser un libre cours à mes péchés :
\v 3 de peur que mes folies ne s’accroissent, que mes péchés ne se multiplient, que je ne tombe en présence de mes adversaires, et que mon ennemi ne se réjouisse à mon sujet ?
\p
\v 4 Seigneur, Père et Dieu de ma vie, ne me donnez pas la licence des yeux,
\v 5 et détournez de moi les désirs mauvais.
\v 6 Que les désirs de la chair et la volupté ne s’emparent pas de moi, et ne me livrez pas à une âme sans pudeur.
\p
\v 7 Mes enfants, écoutez la discipline de la bouche ; celui qui l’observera ne sera pas pris.
\v 8 Par ses lèvres le pécheur sera pris ; le médisant et l’insolent y trouveront une occasion de chute.
\p
\v 9 N’accoutume pas ta bouche à faire des serments, et ne prends pas l’habitude de prononcer le nom du Saint.
\v 10 Car, comme un esclave mis souvent à la torture ne saurait être exempt de meurtrissures, ainsi celui qui fait serment et prononce sans cesse le nom du Saint, ne sera pas pur du péché.
\v 11 L’homme qui fait beaucoup de serments multiplie l’iniquité, et le fouet ne s’éloignera pas de sa maison. S’il s’est rendu coupable, son péché est sur lui ; s’il n’y fait pas attention, son péché est double. S’il a fait un faux serment, il ne sera pas absous, car sa maison sera remplie de châtiments.
\p
\v 12 Il y a des paroles qui appellent la mort : qu’elles ne se rencontrent pas dans l’héritage de Jacob ! Tout cela est éloigné des hommes pieux ; ils ne s’engagent pas dans ces péchés.
\v 13 N’accoutume pas ta bouche à une vile grossièreté, car elle inspire des paroles coupables.
\v 14 Souviens-toi de ton père et de ta mère, quand tu sièges au milieu des grands, de peur que, les oubliant en leur présence, tu ne fasses des sottises par l’effet de l’habitude, et que tu n’en viennes à souhaiter de n’être pas né, et à maudire le jour de ta naissance.
\v 15 Un homme qui s’habitue à un langage grossier ne parviendra jamais à la sagesse.
\p
\v 16 Deux sortes d’hommes multiplient les péchés, et la troisième attire la colère. L’homme que brûle la passion, comme un feu ardent, ne s’éteindra pas jusqu’à ce qu’il soit consumé. L’homme impudique en sa propre chair ne cessera pas jusqu’à ce que le feu soit allumé.
\v 17 Au voluptueux tout pain est doux ; il ne s’arrêtera pas qu’il ne soit mort.
\v 18 L’homme qui quitte la couche conjugale dit dans son cœur : « Qui me voit ? Les ténèbres m’environnent, les murailles me cachent, et personne ne m’aperçoit : que craindrais-je ? Le Très-Haut ne se souviendra pas de mes péchés ». —
\v 19 Les yeux des hommes sont sa crainte, et il ne sait pas que les yeux du Seigneur sont mille fois plus brillants que le soleil ; qu’ils regardent toutes les voies de l’homme, et pénètrent jusque dans les lieux cachés !
\v 20 Avant d’être créées, toutes choses sont connues du Seigneur, elles le sont encore après leur achèvement.
\v 21 L’adultère sera puni dans les rues de la ville, et, là où il ne s’y attendait pas, il sera pris.
\p
\v 22 Il en est de même de la femme qui a abandonné son mari, et donné un hériter d’une union étrangère.
\v 23 Car d’abord elle a désobéi à la loi du Très-Haut : en deuxième lieu, elle a péché envers son mari, et en troisième lieu, elle a commis un adultère, et donné des enfants d’un sang étranger.
\v 24 Elle sera amenée devant l’assemblée, et le châtiment visitera ses enfants.
\v 25 Ses enfants ne pousseront pas de racines, et ses branches ne porteront pas de fruits.
\v 26 Elle laissera une mémoire vouée à la malédiction, et son infamie ne s’effacera jamais.
\v 27 Et les survivants sauront qu’il n’y a rien de meilleur que la crainte du Seigneur, rien de plus doux que d’observer ses commandements. [C’est une grande gloire que de suivre le Seigneur ; s’attacher à lui, c’est la longueur des jours].