\c 39
\p
\v 1 Il en est autrement de celui qui applique son esprit, et qui médite sur la loi du Très-Haut ! Il cherche la sagesse de tous les anciens, et il consacre ses loisirs aux prophéties.
\v 2 Il garde dans sa mémoire les récits des hommes célèbres, et il pénètre dans les détours des sentences.
\v 3 Il cherche le sens caché des proverbes, et il s’occupe des sentences énigmatiques.
\v 4 Il sert au milieu des grands, et il paraît devant le prince. Il voyage dans le pays des peuples étrangers, car il veut connaître le bien et le mal parmi les hommes.
\v 5 Il met son cœur à aller dès le matin auprès du Seigneur qui l’a fait : il prie en présence du Très-Haut, il ouvre sa bouche pour la prière et il demande pardon pour ses péchés.
\p
\v 6 Si c’est la volonté du Seigneur, qui est grand, il sera rempli de l’esprit d’intelligence ; alors il répandra à flots les paroles de la sagesse, et, dans sa prière, il rendra grâces au Seigneur.
\v 7 Il saura diriger sa prudence et son savoir, et il étudiera les mystères divins.
\v 8 Il publiera la doctrine de son enseignement, et il se glorifiera de la loi de l’alliance du Seigneur.
\v 9 Beaucoup loueront son intelligence, et il ne sera jamais oublié ; sa mémoire ne passera pas, et son nom vivra d’âge en âge.
\v 10 Les peuples raconteront sa sagesse, et l’assemblée célébrera ses louanges.
\v 11 Tant qu’il est en vie, son nom reste plus illustre que mille autres ; et, quand il se reposera, sa gloire grandira encore.
\p
\v 12 Je veux encore publier le fruit de mes réflexions, car je suis rempli, comme la lune dans son plein.
\v 13 Ecoutez-moi, fils pieux, et croissez, comme la rose sur le bord d’une eau courante :
\v 14 répandez, comme l’encens, votre suave odeur ; faites éclore votre fleur, comme le lis ; exhalez votre parfum et chantez un cantique, célébrez le Seigneur pour toutes ses œuvres.
\v 15 Rendez gloire à son nom, proclamez sa louange, dans les chants de vos lèvres et sur vos harpes, et, en le célébrant, vous direz :
\p
\v 16 Toutes les œuvres du Seigneur sont très bonnes, et tout ce qu’il a ordonné s’accomplira en son temps.
\v 17 On ne doit pas dire : « Qu’est-ce que cela ? A quoi sert-il ? » car toute chose sera recherchée en son temps. A sa parole, l’eau se rassembla comme un monceau, et, sur une parole de sa bouche, il y eut des réservoirs d’eau.
\v 18 Par son commandement, ce qui lui plaît arrive, et nul ne peut arrêter le salut qu’il envoie.
\p
\v 19 Les œuvres de toute chair sont devant lui, et l’on ne peut se cacher à ses yeux.
\v 20 Son regard atteint de l’éternité à l’éternité, et il n’y a rien d’étonnant devant lui.
\v 21 On ne doit pas dire : « Qu’est-ce que cela ? A quoi sert-il ? » car toute chose a été créée pour son usage.
\v 22 La bénédiction du Seigneur déborde comme un fleuve, et, comme un déluge, elle arrose la terre.
\v 23 De même il donne sa colère en partage aux nations, comme il a changé un pays bien arrosé en une contrée de sel.
\v 24 Ses voies sont droites pour les hommes saints, et de même elles sont pour les impies des occasions de chute.
\p
\v 25 Les biens ont été créés pour les bons dès l’origine, et, de même, les maux pour les méchants.
\v 26 Ce qui est de première nécessité pour la vie de l’homme, c’est l’eau, le feu, le fer et le sel, la farine de froment, le miel et le lait, le sang de la grappe, l’huile et le vêtement.
\v 27 Toutes ces choses sont des biens pour les hommes pieux, mais se changent en maux pour les pécheurs.
\p
\v 28 Il y a des vents qui ont été créés pour la vengeance, et, dans leur fureur, ils déchaînent leurs fléaux. Au jour de la destruction, ils déploieront leur puissance, et apaiseront le courroux de Celui qui les a faits.
\v 29 Feu et grêle, famine et peste, toutes ces choses ont été créées pour le châtiment ;
\v 30 ainsi que la dent des bêtes féroces, les scorpions et les vipères, et le glaive exterminateur funeste aux impies.
\v 31 Ces créatures se réjouissent du commandement du Seigneur, elles se tiennent prêtes sur la terre pour le besoin, et, aux temps marqués, elles ne désobéissent pas à ses ordres.
\p
\v 32 C’est pourquoi j’ai été dès le début ferme dans mes pensées, et, après avoir médité, je les ai mises par écrit :
\v 33 Toutes les œuvres du Seigneur sont bonnes, et il pourvoit à tout besoin en son temps.
\v 34 Il n’y a pas lieu de dire : « Ceci est plus mauvais que cela, » car toute chose en son temps sera reconnue bonne.
\v 35 Et maintenant chantez de tout cœur et de bouche, et bénissez le nom du Seigneur.