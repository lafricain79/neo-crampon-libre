\c 30
\p
\v 1 Celui qui aime son fils lui fait souvent sentir le fouet, afin d’en avoir ensuite de la joie.
\v 2 Celui qui élève bien son fils retirera de lui des avantages, et il se glorifiera de lui devant ses connaissances.
\v 3 Celui qui instruit son fils rendra son ennemi jaloux, et il se réjouira de lui devant ses amis.
\v 4 Son père vient-il à mourir ? C’est comme s’il n’était pas mort, car il laisse après lui quelqu’un qui lui ressemble.
\v 5 Pendant sa vie, il le voit et se réjouit, et, à sa mort, il n’est pas affligé.
\v 6 Il laisse quelqu’un qui le vengera de ses ennemis, et témoignera de la reconnaissance à ses amis.
\p
\v 7 Celui qui gâte son fils bandera ses blessures, et, à chacun de ses cris, ses entrailles seront émues.
\v 8 Le cheval indompté devient intraitable : ainsi le fils abandonné à lui-même devient inconsidéré.
\v 9 Caresse ton enfant, et il te fera trembler, joue avec lui, et il te contristera.
\v 10 Ne ris pas avec lui, de peur que tu n’aies à t’affliger avec lui, et qu’à la fin tu ne grinces des dents.
\v 11 Ne lui donne pas toute liberté dans sa jeunesse, et ne ferme pas les yeux sur ses folies.
\v 12 [Fais plier sa tête pendant sa jeunesse,] et meurtris-lui les flancs pendant qu’il est enfant, de peur qu’il ne devienne opiniâtre et ne t’obéisse plus, [et que tu n’aies la douleur au cœur.]
\v 13 Corrige ton fils, et fais-le travailler, de peur qu’il ne trébuche par ta honteuse faiblesse.
\p
\v 14 Mieux vaut un pauvre sain et vigoureux, qu’un riche flagellé dans son corps par la maladie.
\v 15 La santé et la bonne complexion valent mieux que tout l’or, et un corps vigoureux est préférable à une immense fortune.
\v 16 Il n’y a pas de richesse préférable à la santé du corps, et il n’y a pas de joie meilleure que la joie du cœur.
\v 17 Mieux vaut la mort qu’une vie d’amertume, et l’éternel repos qu’une souffrance continuelle.
\v 18 Des biens répandus sur une bouche fermée sont comme les offrandes d’aliments placés sur une tombe.
\v 19 Que sert l’offrande à une idole ? Elle ne la mangera pas et n’en sentira pas l’odeur :
\v 20 ainsi en est-il de l’homme que Dieu poursuit par la maladie : Il voit de ses yeux, et il soupire, comme soupire un eunuque qui tient une vierge dans ses bras.
\p
\v 21 N’abandonne pas ton âme à la tristesse, et ne te tourmente pas par tes réflexions.
\v 22 La joie au cœur est la vie de l’homme, et l’allégresse de l’homme est pour lui longueur de jours.
\v 23 Aime ton âme et console ton cœur, et chasse loin de toi la tristesse ; le chagrin en a tué beaucoup, et il n’y a pas en lui de profit.
\v 24 L’emportement et la colère abrègent les jours, et les soucis amènent la vieillesse avant le temps.
\v 25 Le cœur généreux et bon prend soin des mets qui forment sa nourriture.