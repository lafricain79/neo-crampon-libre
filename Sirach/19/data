\c 19
\p
\v 1 L’ouvrier adonné au vin ne s’enrichira pas ; celui qui ne soigne pas le peu qu’il a tombera bientôt dans la ruine.
\p
\v 2 Le vin et les femmes égarent les hommes intelligents, et celui qui s’attache aux courtisanes est un imprudent.
\v 3 Les larves et les vers en feront leur proie, et l’âme criminelle sera retranchée.
\p
\v 4 Celui qui croit trop vite est un cœur léger, et celui qui tombe dans cette faute pèche contre son âme.
\v 5 Celui qui prend plaisir à de sots discours sera condamné,
\v 6 et celui qui hait le bavardage se préserve du mal.
\p
\v 7 Ne répète jamais une parole, et tu n’encourras aucun dommage.
\v 8 Ne la redis ni à un ami ni à un ennemi, et, à moins qu’il n’y ait faute pour toi, ne la révèle pas.
\v 9 Car, s’il t’entend, il se gardera de toi, et, le moment venu, il se montrera ton ennemi.
\p
\v 10 As-tu entendu quelque grave propos ? Qu’il meure avec toi ! Sois sans inquiétude, il ne te fera pas éclater.
\v 11 Pour une parole à garder, l’insensé est dans les douleurs, comme la femme en travail d’enfant.
\v 12 Comme une flèche enfoncée dans la chair de la cuisse, ainsi est une parole dans le cœur de l’insensé.
\p
\v 13 Questionne ton ami ; peut-être n’a-t-il pas fait la chose ; et, s’il l’a faite, afin qu’il ne la fasse plus.
\v 14 Questionne ton ami ; peut-être n’a-t-il pas dit la chose ; et, s’il l’a dite, afin qu’il ne recommence pas.
\v 15 Questionne ton ami, car souvent il y a calomnie, et ne crois pas tout ce qu’on dit.
\v 16 Il en est qui manquent, mais sans que le cœur y soit ; et qui est-ce qui n’a pas péché par sa langue ?
\v 17 Questionne ton ami avant d’en venir aux menaces, et attache-toi à observer la loi du Très-Haut.
\p
\v 18 Toute sagesse consiste dans la crainte du Seigneur, et dans toute sagesse est l’accomplissement de la loi.
\p
\v 19 La sagesse n’est pas l’habileté à faire le mal, et la prudence ne se trouve pas dans le conseil des pécheurs.
\v 20 Il y a une habileté qui est exécrable, et il y a une folie qui n’est qu’un manque de sagesse.
\v 21 Mieux vaut celui qui a peu d’intelligence et qui craint Dieu, que l’homme qui a beaucoup de sens et qui transgresse la loi.
\v 22 Il y a une habileté véritable, mais qui viole la justice, et il est tel qui fausse la cause pour faire rendre la sentence qu’il désire.
\p
\v 23 Il est tel méchant qui marche courbé par le chagrin, et son cœur est rempli de fraude.
\v 24 Il baisse la tête, il est sourd d’un côté, et, dès qu’il n’est pas remarqué, il prend sur toi les devants.
\v 25 Et si, par sa faiblesse, il est empêché de pécher, il fera le mal quand il en trouvera l’occasion.
\p
\v 26 A son air on connaît un homme, et au visage qu’il présente on connaît le sage.
\v 27 Le vêtement d’un homme, le rire de ses lèvres, et la démarche d’un homme révèlent ce qu’il est.