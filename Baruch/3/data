\c 3
\p
\v 1 Seigneur tout-puissant, Dieu d’Israël, une âme dans l’angoisse et un esprit inquiet crie vers vous.
\v 2 Écoutez, Seigneur, et ayez pitié, parce que nous avons péché devant vous ;
\v 3 car vous êtes assis sur un trône éternel, et nous, nous périssons sans retour.
\v 4 Seigneur tout-puissant, Dieu d’Israël, écoutez la prière des morts d’Israël, et des fils de ceux qui ont péché devant vous, eux qui n’ont pas écouté la voix de leur Dieu, et sont cause que ces malheurs se sont attachés à nous.
\v 5 Ne vous souvenez plus des iniquités de nos pères, mais souvenez-vous, à cette heure, de votre puissance et de votre nom.
\v 6 Car vous êtes le Seigneur, notre Dieu, et nous vous louerons, Seigneur.
\v 7 C’est pour cela que vous avez mis votre crainte dans nos cœurs, pour que nous invoquions votre nom ; et nous vous louerons dans notre exil ; car nous avons écarté de nos cœurs l’iniquité de nos pères, qui ont péché devant vous.
\v 8 Voici que nous sommes aujourd’hui dans notre terre d’exil, où vous nous avez dispersés pour l’opprobre, la malédiction et l’expiation, selon toutes les iniquités de nos pères, qui se sont détournés du Seigneur, notre Dieu.
\p
\v 9 Écoute, Israël, les commandements de vie ; prête l’oreille pour apprendre la prudence.
\v 10 D’où vient, Israël, d’où vient que tu es dans le pays de tes ennemis, que tu languis sur une terre étrangère, que tu te souilles avec les morts,
\v 11 et que tu es compté parmi ceux qui sont descendus au schéol ?
\v 12 Tu as abandonné la source de la sagesse.
\v 13 Car si tu avais marché dans la voie de Dieu, tu habiterais à jamais dans la paix.
\v 14 Apprends où est la prudence, où est la force, où est l’intelligence, afin que tu saches en même temps où est la longueur des jours et la vie, où est la lumière des yeux et la paix.
\p
\v 15 Qui a trouvé le lieu de la sagesse, et qui est entré dans ses trésors ?
\v 16 Où sont les chefs des nations, et ceux qui domptent les bêtes de la terre,
\v 17 qui se jouent des oiseaux du ciel,
\v 18 qui amassent l’argent et l’or, dans lesquels les hommes mettent leur confiance, et dont les possessions n’ont pas de fin ?
\p Car ces hommes qui amassent de l’argent et en font leur souci, on ne trouverait plus trace de leurs œuvres.
\v 19 Ils ont disparu et sont descendus au schéol, et d’autres se sont élevés à leur place.
\v 20 Des jeunes gens ont vu la lumière, et ont habité sur la terre ; mais ils n’ont pas connu le chemin de la sagesse ;
\v 21 ils n’ont pas connu ses sentiers ; leurs fils non plus ne l’ont pas saisie ; ils étaient loin de sa voie !
\p
\v 22 On n’a pas entendu parler d’elle au pays de Canaan, et elle n’a pas été vue dans Théman.
\v 23 Et les fils d’Agar qui cherchent la prudence qui est de la terre ; les marchands de Merrha et de Théman, les interprètes de paraboles et les chercheurs de prudence n’ont pas connu la voie de la sagesse, et ne se sont pas souvenus de ses sentiers.
\p
\v 24 O Israël, qu’elle est grande la maison de Dieu, qu’il est vaste le lieu de son domaine !
\v 25 Il est vaste et n’a pas de bornes, il est élevé et immense.
\p
\v 26 Là vécurent dès l’origine les géants fameux, à la haute stature et habiles dans la guerre.
\v 27 Ce n’est pas eux que Dieu a choisis, et il ne leur a pas appris le chemin de la sagesse.
\v 28 Et ils ont péri parce qu’ils n’avaient pas la vraie science, ils ont péri à cause de leur folie.
\p
\v 29 Qui est monté au ciel et a saisi la sagesse, et l’a fait descendre des nuées ?
\v 30 Qui a passé la mer, et, l’ayant trouvée, l’a rapportée au prix de l’or le plus pur ?
\v 31 Il n’y a personne qui connaisse ses voies, ni qui observe ses sentiers.
\v 32 Mais celui qui sait toutes choses la connaît, il la découvre par sa prudence, celui qui a affermi la terre à jamais, et qui l’a remplie d’animaux à quatre pieds,
\v 33 qui envoie la lumière, et elle part, qui l’appelle, et elle lui obéit en tremblant.
\v 34 Les étoiles brillent à leurs postes, et elles sont dans la joie ;
\v 35 il les appelle, et elles disent : « Nous voici ! » et elles brillent joyeusement pour celui qui les a créées.
\p
\v 36 C’est lui qui est notre Dieu, et nul autre ne lui est comparable.
\v 37 Il a trouvé toutes les voies de la sagesse, et il l’a donnée à Jacob, son serviteur, et à Israël son bien-aimé.
\v 38 Après cela il a apparu sur la terre, et il a conversé parmi les hommes.