\c 7
\p
\v 1 L’an cent cinquante et un, Démétrius, fils de Séleucus, s’échappa de la ville de Rome et aborda, avec un petit nombre de gens, dans une ville maritime où il prit le titre de roi.
\v 2 Dès qu’il eut fait son entrée dans le royaume de ses pères, l’armée se saisit d’Antiochus et de Lysias pour les lui amener.
\v 3 Lorsqu’il en fut averti, il dit : « Ne me faites pas voir leur visage. »
\v 4 Alors l’armée les tua, et Démétrius s’assit sur le trône de son royaume.
\p
\v 5 Alors tous les hommes iniques et impies d’Israël vinrent le trouver, conduits par Alcime, qui voulait être grand prêtre.
\v 6 Ils accusèrent le peuple auprès du roi en disant : « Judas et ses frères ont fait périr tous tes amis, et nous ont expulsés de notre terre.
\v 7 Envoie donc maintenant un homme en qui tu aies confiance, pour qu’il aille constater toute la ruine qu’ils ont faite parmi nous et dans les provinces du roi, et qu’il punisse les coupables avec tous ceux qui leur viennent en aide. »
\p
\v 8 Le roi choisit parmi ses amis Bacchidès, gouverneur du pays situé au-delà du fleuve, homme très considérable dans le royaume et fidèle au roi ;
\v 9 et il l’envoya avec l’impie Alcime, auquel il assura la souveraine sacrificature, et lui ordonna de tirer vengeance des enfants d’Israël.
\v 10 S’étant mis en route, ils vinrent avec une grande armée dans le pays de Juda, et ils envoyèrent des messagers porter à Judas et à ses frères des paroles de paix, pour les tromper.
\v 11 Mais ceux-ci, voyant qu’ils étaient venus avec une grande armée, n’écoutèrent pas leur discours.
\v 12 Cependant une troupe de scribes se rendit auprès d’Alcime et de Bacchidès pour chercher le droit ;
\v 13 et ceux qui tenaient le premier rang parmi les enfants d’Israël, les Assidéens, leur demandèrent la paix ;
\v 14 car ils disaient : « Un prêtre de la race d’Aaron est venu avec l’armée ; il ne saurait nous maltraiter. »
\v 15 Il leur fit entendre des paroles de paix et leur fit ce serment : « Nous ne voulons vous faire aucun mal, ni à vous, ni à vos amis. »
\v 16 Ils le crurent ; mais lui fit saisir soixante d’entre eux et les fit massacrer le même jour, selon la parole de l’Ecriture :
\v 17 « Ils ont dispersé la chair et répandu le sang de tes saints autour de Jérusalem, et il n’y a personne pour les ensevelir. »
\v 18 Alors la crainte et la terreur s’emparèrent de tout le peuple : « Il n’y a plus, disait-on, ni vérité ni justice parmi eux, car ils ont violé leur engagement et le serment qu’ils avaient fait. »
\p
\v 19 Bacchidès partit de Jérusalem et alla camper à Bézeth ; là il envoya saisir un grand nombre de ceux qui avaient déserté son parti, avec quelques-uns du peuple, et les ayant tués, il jeta leurs cadavres dans la grande citerne.
\v 20 Après avoir confié le pays à Alcime, en lui laissant des troupes pour le défendre, Bacchidès s’en retourna auprès du roi.
\v 21 Alcime s’efforça de se mettre en possession du pontificat.
\v 22 Tous ceux qui troublaient leur peuple s’assemblèrent autour de lui, se rendirent maîtres du pays de Juda et causèrent une grande affliction en Israël.
\v 23 Voyant tous les maux que faisaient aux enfants d’Israël Alcime et ses partisans, plus funestes que les Gentils eux-mêmes,
\v 24 Judas parcourut en tout sens le territoire de la Judée, châtiant les apostats et les empêchant de se répandre dans les campagnes.
\v 25 Lorsque Alcime vit que Judas et ses compagnons étaient devenus puissants, reconnaissant qu’il ne pouvait tenir contre eux, il retourna auprès du roi et les accusa des plus grands méfaits.
\p
\v 26 Le roi envoya Nicanor, un de ses plus illustres généraux, rempli de haine et d’animosité contre Israël, avec ordre d’exterminer le peuple.
\v 27 Arrivé à Jérusalem avec une forte armée, Nicanor fit adresser à Judas et à ses frères des paroles de paix, pour les tromper :
\v 28 « Qu’il n’y ait pas, disait-il, de guerre entre vous et moi ; je veux aller avec un petit nombre d’hommes voir vos visages en amitié. »
\v 29 Il vint donc vers Judas, et ils se saluèrent mutuellement avec des démonstrations amicales ; mais les ennemis étaient prêts à se saisir de Judas.
\v 30 Informé que Nicanor était venu le trouver dans un but perfide, Judas effrayé se retira et refusa de le voir davantage.
\v 31 Nicanor reconnut alors que son projet était découvert, et il en vint immédiatement aux armes contre Judas près de Capharsalama.
\v 32 Environ cinq mille hommes de l’armée de Nicanor furent tués ; le reste s’enfuit dans la ville de David.
\p
\v 33 Après ces événements, Nicanor étant monté au mont Sion, quelques-uns des prêtres sortirent du lieu saint, accompagnés de plusieurs anciens du peuple, pour le saluer amicalement et lui montrer les holocaustes qui étaient offerts pour le roi.
\v 34 Mais lui, les raillant et les traitant avec mépris, les souilla et prononça des paroles insolentes ;
\v 35 et il fit ce serment avec colère : « Si Judas et son armée ne sont pas livrés sur le champ entre mes mains, dès que je serai revenu en paix, je brûlerai cet édifice. » Et il sortit tout en colère.
\v 36 Alors les prêtres rentrèrent et, se tenant devant l’autel et le sanctuaire, ils dirent en pleurant :
\v 37 « C’est vous, Seigneur, qui avez choisi cette maison pour y mettre votre nom, afin qu’elle fût pour votre peuple une maison de prière et de supplication.
\v 38 Tirez vengeance de cet homme et de son armée, et qu’ils tombent par l’épée ! Souvenez-vous de leurs blasphèmes, et ne permettez pas qu’ils demeurent ! »
\p
\v 39 Nicanor, quittant Jérusalem, alla camper près de Béthoron, et un corps de Syriens vint au-devant de lui.
\v 40 Judas, de son côté, campa près d’Adasa avec trois mille hommes, et il pria en disant :
\v 41 « Ceux qui avaient été envoyés par le roi des Assyriens vous ayant blasphémé, Seigneur, votre ange vint et leur tua cent quatre vingt-cinq mille hommes.
\v 42 Exterminez de même en ce jour cette armée en notre présence, afin que tous les autres reconnaissent qu’il a tenu un langage impie sur votre sanctuaire, et jugez-le selon sa méchanceté. »
\v 43 Les armées en vinrent aux mains le treizième jour du mois d’Adar, et les troupes de Nicanor furent taillées en pièces ; lui-même tomba le premier dans le combat.
\v 44 Les troupes, voyant que Nicanor était tombé, jetèrent leurs armes et prirent la fuite.
\v 45 Les Juifs les poursuivirent une journée de chemin, depuis Adasa jusqu’aux environs de Gazara, sonnant derrière eux les trompettes en fanfare.
\v 46 De tous les villages de Judée aux alentours sortirent des gens qui enveloppèrent les Syriens : ceux-ci alors se retournaient les uns sur les autres, et tous tombèrent par l’épée, sans qu’aucun d’eux échappât, pas même un seul.
\v 47 Ils prirent les dépouilles des vaincus, ainsi que leur butin ; et ayant coupé la tête de Nicanor et sa main droite, qu’il avait insolemment étendue, ils les apportèrent et les suspendirent en vue de Jérusalem.
\v 48 Le peuple fut rempli de joie, et ils célébrèrent ce jour comme un jour de grande allégresse.
\v 49 On décida que ce jour serait célébré chaque année, le treize du mois d’Adar.
\p
\v 50 Et le pays de Juda fut tranquille pendant un peu de temps.